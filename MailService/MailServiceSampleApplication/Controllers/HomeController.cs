﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailService.Interface;
using Microsoft.AspNetCore.Mvc;

namespace MailServiceSampleApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMailer mailer;

        public HomeController(IMailer mailer)
        {
            this.mailer = mailer;
        }
        public IActionResult Index()
        {
            try
            {
                List<string> toMail = new List<string>() { "ab.alaminshaon@gmail.com","ashikul58bd@gmail.com" };
                mailer.SendEmailAsync(toMail, "MailFromPC", "Test Mail").Wait();
                return Json("Message Sent Successfully");
            }
            catch (Exception)
            {
                return Json("Failed");
            }
            
        }
    }
}