﻿using MailKit.Security;
using MailKit.Net.Smtp;
using MailService.Interface;
using MailService.Model;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace MailService.Services
{
    public class Mailer: IMailer
    {
        private readonly SmtpSettings smtpSettings;

        public Mailer(IOptions<SmtpSettings> smtpSettings)
        {
            this.smtpSettings = smtpSettings.Value;
        }

        public async Task SendEmailAsync(List<string> toMail, string subject, string body)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(MailboxAddress.Parse(smtpSettings.SenderEmail));
                foreach (var item in toMail)
                {
                    message.To.Add(MailboxAddress.Parse(item));
                }
                message.Subject = subject;
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = body
                };

                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    await client.ConnectAsync(smtpSettings.Server, smtpSettings.Port, SecureSocketOptions.StartTls);
                    await client.AuthenticateAsync(smtpSettings.SenderEmail, smtpSettings.Password);
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
