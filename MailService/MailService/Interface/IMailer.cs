﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MailService.Interface
{
    public interface IMailer
    {
        Task SendEmailAsync(List<string> toMail, string subject, string body);
    }
}
